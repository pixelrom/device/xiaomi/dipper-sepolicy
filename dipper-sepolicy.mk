PRODUCT_PUBLIC_SEPOLICY_DIRS := device/xiaomi/dipper-sepolicy/public
PRODUCT_PRIVATE_SEPOLICY_DIRS := device/xiaomi/dipper-sepolicy/private vendor/google/security/private

# vendors
BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor

ifneq ($(filter dipper polaris,$(TARGET_DEVICE)),)
BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/nfc
endif

# QCOM-specific policy
include device/xiaomi/dipper-sepolicy/vendor/qcom/sepolicy.mk
