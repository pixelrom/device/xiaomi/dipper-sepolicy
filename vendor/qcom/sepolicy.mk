# Board specific SELinux policy variable definitions
BOARD_PLAT_PUBLIC_SEPOLICY_DIR += \
    device/xiaomi/dipper-sepolicy/vendor/qcom/generic/public

BOARD_PLAT_PRIVATE_SEPOLICY_DIR += \
    device/xiaomi/dipper-sepolicy/vendor/qcom/generic/private

BOARD_PLAT_PUBLIC_SEPOLICY_DIR += \
    device/xiaomi/dipper-sepolicy/vendor/qcom/qva/public

BOARD_PLAT_PRIVATE_SEPOLICY_DIR += \
    device/xiaomi/dipper-sepolicy/vendor/qcom/qva/private

# sepolicy rules for product images
PRODUCT_PUBLIC_SEPOLICY_DIRS += \
    device/xiaomi/dipper-sepolicy/vendor/qcom/product/public

PRODUCT_PRIVATE_SEPOLICY_DIRS += \
    device/xiaomi/dipper-sepolicy/vendor/qcom/product/private

ifeq (,$(filter sdm845 sdm710, $(TARGET_BOARD_PLATFORM)))
    BOARD_SEPOLICY_DIRS += \
        device/xiaomi/dipper-sepolicy/vendor/qcom/generic/vendor/common \
        device/xiaomi/dipper-sepolicy/vendor/qcom/qva/vendor/common/sysmonapp \
        device/xiaomi/dipper-sepolicy/vendor/qcom/qva/vendor/ssg \
        device/xiaomi/dipper-sepolicy/vendor/qcom/timeservice \
        device/xiaomi/dipper-sepolicy/vendor/qcom/qva/vendor/qwesas \
        device/xiaomi/dipper-sepolicy/vendor/qcom/qva/vendor/common

    ifeq ($(TARGET_SEPOLICY_DIR),)
      BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor/qcom/generic/vendor/$(TARGET_BOARD_PLATFORM)
      BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor/qcom/qva/vendor/$(TARGET_BOARD_PLATFORM)
    else
      BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor/qcom/generic/vendor/$(TARGET_SEPOLICY_DIR)
      BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor/qcom/qva/vendor/$(TARGET_SEPOLICY_DIR)
    endif

    ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
      BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor/qcom/generic/vendor/test
      BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor/qcom/qva/vendor/test
    endif
endif

ifneq (,$(filter sdm845 sdm710, $(TARGET_BOARD_PLATFORM)))
    BOARD_SEPOLICY_DIRS += \
        device/xiaomi/dipper-sepolicy/vendor/qcom/legacy/vendor/common/sysmonapp \
        device/xiaomi/dipper-sepolicy/vendor/qcom/legacy/vendor/ssg \
        device/xiaomi/dipper-sepolicy/vendor/qcom/timeservice \
        device/xiaomi/dipper-sepolicy/vendor/qcom/legacy/vendor/common

    ifeq ($(TARGET_SEPOLICY_DIR),)
      BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor/qcom/legacy/vendor/$(TARGET_BOARD_PLATFORM)
    else
      BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor/qcom/legacy/vendor/$(TARGET_SEPOLICY_DIR)
    endif
    ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
      BOARD_SEPOLICY_DIRS += device/xiaomi/dipper-sepolicy/vendor/qcom/legacy/vendor/test
    endif
endif
